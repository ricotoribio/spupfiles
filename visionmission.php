<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<title>SPUP</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>		


			<!-- Page Title -->
			<div class="container" style="margin-top: 0px">
	 	 		<div class="jumbotron">
	 		 		<img src="image/spuplogo.png" alt="" style="height: 100px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h3 style="text-align: center">St. Paul University Philippines</h3>
	  			</div>	
			</div>

			<!-- Menu Bar -->
			<div class="container" style="margin-top: -30px">
				<nav class="navbar navbar-inverse">
				  <div class="container-fluid" >
				    <div class="navbar-header">
				    </div>
				    <ul class="nav navbar-nav">
				      <li class="active"><a href="index.php">Home</a></li>
				      <li><a href="history.php">History</a></li>
				      <li><a href="awards.php">Awards and Citations</a></li>
				      <li><a href="visionmission.php">Vision-Mission and Quality Policies</a></li>
				      <li><a href="core.php">Core Values</a></li>
				      <li><a href="hymn.php">Paulinian Hymn</a></li>
				    </ul>
				  </div>
				</nav>
			</div>

			<div class="container">
      			<h3 style="text-align: center";font face="verdana" color="green">Vision</h3>        
      				<p style="text-align: center">St. Paul University Philippines is an internationally recognized institution dedicated to the formation of competent leaders and responsible citizens of their communities, country, and the world.</p>
      			<h3 style="text-align: center";font face="verdana" color="green">Mission</h3>
      				<p style="text-align: center">Animated by the gospel and guided by the teachings of the Church, it helps to uplift the quality of life and to effect social transformation through:</p>
  					<p style="text-align: center">1. Quality, Catholic, Paulinian formation, academic excellence, research, and community services;</p>
  					<p style="text-align: center">2. Optimum access to Paulinian education and service in an atmosphere of compassionate caring;    and</p>
  					<p style="text-align: center">3. Responsive and innovative management processes.</p>
  				<h4 style="text-align: center">Quality Policy</h4>
  				<h3 style="text-align: center">Caritas Christi Urget Nos!</h3>
    		</div>

	<div class="container">
		<div class="footer">
	 	 	<div class="jumbotron">
	 	 	<p style="text-align: center;margin-top: -40px"> Accreditations </p>
	 	 	<div class="row">
	 		 	<div class="col-sm-2" style="margin-right: 35px"><img src="image/iso.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">ISO</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/iao.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">IAO</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/aasbi.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">AASBI</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/paascu.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">PAASCU</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/pacucoa.png" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">PACUCOA</h4>
	    		</div>
	  		</div>	
	</div>


</body>
</html>