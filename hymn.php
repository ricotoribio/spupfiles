<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<title>SPUP</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>		


			<!-- Page Title -->
			<div class="container" style="margin-top: 0px">
	 	 		<div class="jumbotron">
	 		 		<img src="image/spuplogo.png" alt="" style="height: 100px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h3 style="text-align: center">St. Paul University Philippines</h3>
	  			</div>	
			</div>

			<!-- Menu Bar -->
			<div class="container" style="margin-top: -30px">
				<nav class="navbar navbar-inverse">
				  <div class="container-fluid" >
				    <div class="navbar-header">
				    </div>
				    <ul class="nav navbar-nav">
				      <li class="active"><a href="index.php">Home</a></li>
				      <li><a href="history.php">History</a></li>
				      <li><a href="awards.php">Awards and Citations</a></li>
				      <li><a href="visionmission.php">Vision-Mission and Quality Policies</a></li>
				      <li><a href="core.php">Core Values</a></li>
				      <li><a href="hymn.php">Paulinian Hymn</a></li>
				    </ul>
				  </div>
				</nav>
			</div>

			<div class="container">
					<h2 style="text-align: center"><b>Paulinian Hymn</b><h2>
					<h4 style="text-align: center">Hark! Sons and Daughters of St. Paul,</h4>
					<h4 style="text-align: center">Come listen to his call.</h4>
					<h4 style="text-align: center">O children of this loved school,</h4>
					<h4 style="text-align: center">The loving nurse of all.</h4>
					<h4 style="text-align: center">Rejoice in God, do work and play,</h4>
					<h4 style="text-align: center">Be true from day to day;</h4>
					<h4 style="text-align: center">Beloved school of mine,</h4>
					<h4 style="text-align: center">My pains and joys are thine.</h4>
					<h4 style="text-align: center">My childhood’s early dreams are closely linked with thee;</h4>
					<h4 style="text-align: center">The hope that heaven brings</h4>
					<h4 style="text-align: center">Thou dost unfold to me. (2x)</h4>
					<h4 style="text-align: center">Sweet are the days of childhood,</h4>
					<h4 style="text-align: center">With friends we love and care;</h4>
					<h4 style="text-align: center">Those golden years of childhood</h4>
					<h4 style="text-align: center">Whose sympathy we share,</h4>
					<h4 style="text-align: center">Do stay and while the hours away,</h4>
					<h4 style="text-align: center">With us in work and play,</h4>
					<h4 style="text-align: center">And when we leave our dear old school,</h4>
					<h4 style="text-align: center">These mem’ries we’ll recall. (2x)</h4>
					<h4 style="text-align: center">(Repeat Refrain)</h4>

    		</div>

	<div class="container">
		<div class="footer">
	 	 	<div class="jumbotron">
	 	 	<p style="text-align: center;margin-top: -40px"> Accreditations </p>
	 	 	<div class="row">
	 		 	<div class="col-sm-2" style="margin-right: 35px"><img src="image/iso.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">ISO</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/iao.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">IAO</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/aasbi.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">AASBI</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/paascu.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">PAASCU</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/pacucoa.png" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">PACUCOA</h4>
	    		</div>
	  		</div>	
	</div>


</body>
</html>