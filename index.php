<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<title>SPUP</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>		


			<!-- Page Title -->
			<div class="container" style="margin-top: 0px">
	 	 		<div class="jumbotron">
	 		 		<img src="image/spuplogo.png" alt="" style="height: 100px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h3 style="text-align: center">St. Paul University Philippines</h3>
	  			</div>	
			</div>

			<!-- Menu Bar (dito niyo lagay yung link ng gagawin niyo)-->
			<div class="container" style="margin-top: -30px">
				<nav class="navbar navbar-inverse">
				  <div class="container-fluid">
				    <div class="navbar-header">
				    </div>
				    <ul class="nav navbar-nav">
				      <li class="active"><a href="index.php">Home</a></li>
				      <li><a href="history.php">History</a></li>
				      <li><a href="awards.php">Awards and Citations</a></li>
				      <li><a href="visionmission.php">Vision-Mission and Quality Policies</a></li>
				      <li><a href="core.php">Core Values</a></li>
				      <li><a href="hymn.php">Paulinian Hymn</a></li>
				    </ul>
				  </div>
				</nav>
			</div>
		


			<!-- News -->
			<div class="container" style="text-align: center">
				<div class="container-fluid">
					<div class="row">
						<h1 style="text-align: center;text-align: center;border-style: solid;border-color: #87ceeb"> News </h1>

						<!-- News1 -->
						 <div class="col-sm-4"><img src="image/news1.jpg" alt="" style="height: 100px;width: 200px;float:middle;display:block; margin-left: auto; margin-right: auto;">
						 		<h4 style="text-align:center">SPUP-MEDTECH YIELDS 90.91% PASSING RATE</h4></br><p style="text-align: center;margin-top:-20px">2017-09-03</p></br>
								<button data-toggle="collapse" data-target="#demo">Read More</button>

								<div id="demo" class="collapse">
									Caritas Christi urget nos!</br></br></br>

									SPUP's College of Medical Technology (MedTech) registered a passing rate of 90.91%. This was announced by the Professional Regulation Commission (PRC) as it released the result of the Medical Technology Licensure Examinations on August 31,2017. The Commission also published that this year's MedTech Exams yielded a national passing rate of 85.16%. This meant that 4,821 out of 5,661 examinees passed the board exam which was conducted last August 26-27,2017.
								</div>
						  </div>

						  <!-- News2 -->
						  <div class="col-sm-4"><img src="image/news2.jpg" alt="" style="height: 100px;width: 200px;float:middle;display:block; margin-left: auto; margin-right: auto;">
						 			<h4 style="text-align:center">SPUP’S PVCD REAPS AWARDS IN UNESCO CLUBS INTERNATIONAL ASSEMBLY</h4></br><p style="text-align: center;margin-top:-20px">2017-09-03</p></br>
									<button data-toggle="collapse" data-target="#news2">Read More</button>

								<div id="news2" class="collapse">
									<p>Caritas Christi urget nos!</br></br></br>

									(by: Ms. Noemi Cabaddu and Dr. Allan Peejay Lappay)</br></br></br>

									The Paulinian Volunteers for Community Development (PVCD) Club was honoured as OUTSTANDING UNESCO CLUB (Education Category) and obtained a Certificate of GOOD STANDING during the annual assembly of UNESCO Clubs in the Philippines. Moreover, Mrs. Noemi Cabaddu (CES Director and PVCD Adviser) was awarded OUTSTANDING UNESCO CLUB EDUCATOR; while, Ms. Cheeni Mabbayad (PVCD President) was hailed as OUTSTANDING UNESCO CLUB YOUTH LEADER. The awards were presented during the 2017 International Assembly of Youth for UNESCO held at the Icon Hotel, Quezon City on September 01-03, 2017.</br></br>

									The awarding served as one of the highlights of this year’s conference organized by the National Association of UNESCO Clubs in the Philippines (NAUCP), which was participated in by more than 300 participants from the country and abroad. Mr. Jonathan Guerero (President, NAUCP) and Dr. Serafin Arviola, Jr. (Chairman, NAUCP) led the recognition of the awardees.</br></br>

									(SPUP's PVCD is the FIRST UNESCO-Recognized Student CLUB in REGION II and the ONLY in the St. Paul University System. It is also the ONLY RECIPIENT from the Cagayan Valley Region of this year's Outstanding award.)</p>
								</div>
						  </div>


						  <!-- New3 -->
						  <div class="col-sm-4"><img src="image/news3.jpg" alt="" style="height: 100px;width: 200px;float:middle;display:block; margin-left: auto; margin-right: auto;">
						 		<h4 style="text-align:center">SPUP STUDENT-LEADERS PARTICIPATE IN TAIWAN’S GLOBAL CULTURAL EXCHANGE SUMMER CAMP</h4></br><p style="text-align: center;margin-top:-20px">2017-08-26</p></br>
								<button data-toggle="collapse" data-target="#news3">Read More</button>

								<div id="news3" class="collapse">
									Caritas Christi urget nos!</br></br></br>

									(by: Allaine Azada)</br></br></br>

									Three Paulinian student-leaders joined the recently concluded 2017 Global Cultural Exchange Summer Camp at Chang Jung Christian University (CJCU), Tainan City, Taiwan, last August 21-26, 2017. SPUP was represented by the officers of the Paulinian Student Government (PSG) namely: Linus Janvier Pagulayan, (President), Fatima Sheryl Agcaoili (Executive Secretary), and Allaine Azada (Chief Justice).</br></br>

									The summer camp was organized by the Language Education Center and the Office of International Affairs at CJCU. It was designed to foster understanding regarding cultural diversity, sport a global vision and foster closer ties among the participants from different Asian countries.</br></br>

									The activity featured CJCU-Spotlight Social Business where groups drafted and thought of businesses which considers social mission as its core to success. It was facilitated by newly founded Taiwanese branch of Yunus Social Business Center at CJCU. Moreover, the participants had also engaged in Taiwan art and history, do-it-yourself Taiwan cuisine, innovation and creativity workshops, cultural boot camp presentations, and trips around Taiwan.</br></br>

									The summer camp was graced by 78 students who had undergone a prerequisite application process— 33 freshmen students from CJCU, and 45 foreign students coming from Philippines, Japan, Indonesia, Malaysia, Thailand, South Korea, India.
								</div>
						  </div>
					</div>
				</div>
			</div>

			<!-- Footer -->
			<div class="container" style="margin-top: 10px">
				<div class="footer">
			 	 	<div class="jumbotron">
			 	 	<p style="text-align: center;margin-top: -40px"> Accreditations </p>
			 	 	<div class="row">
			 		 	<div class="col-sm-2" style="margin-right: 35px"><img src="image/iso.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
			    			<h4 style="text-align: center">ISO</h4>
			    		</div>

			    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/iao.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
			    			<h4 style="text-align: center">IAO</h4>
			    		</div>

			    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/aasbi.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
			    			<h4 style="text-align: center">AASBI</h4>
			    		</div>

			    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/paascu.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
			    			<h4 style="text-align: center">PAASCU</h4>
			    		</div>

			    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/pacucoa.png" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
			    			<h4 style="text-align: center">PACUCOA</h4>
			    		</div>
			  		</div>	
			</div>
</body>
</html>