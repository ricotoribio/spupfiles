<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<title>SPUP</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>		


			<!-- Page Title -->
			<div class="container" style="margin-top: 0px">
	 	 		<div class="jumbotron">
	 		 		<img src="image/spuplogo.png" alt="" style="height: 100px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h3 style="text-align: center">St. Paul University Philippines</h3>
	  			</div>	
			</div>

			<!-- Menu Bar -->
			<div class="container" style="margin-top: -30px">
				<nav class="navbar navbar-inverse">
				  <div class="container-fluid" >
				    <div class="navbar-header">
				    </div>
				    <ul class="nav navbar-nav">
				      <li class="active"><a href="index.php">Home</a></li>
				      <li><a href="history.php">History</a></li>
				      <li><a href="awards.php">Awards and Citations</a></li>
				      <li><a href="visionmission.php">Vision-Mission and Quality Policies</a></li>
				      <li><a href="core.php">Core Values</a></li>
				      <li><a href="hymn.php">Paulinian Hymn</a></li>
				    </ul>
				  </div>
				</nav>
			</div>
		



	<div class="container">
		<div class="container-fluid">
		<h1 style="text-align: center;border-style: solid;border-color: #87ceeb">Core Values</h1>
		<h4><b>Christ-Centeredness. </b> Christ is the center of Paulinian life; he/she follows and imitates Christ, doing everything in reference to Him.</h4>


		<h4><b>Commission.</b> The Paulinian has a mission - a life purpose to spread the Good News; like Christ, he/she actively works "to save" this world, to make it better place to live in.</h4>

		<h4><b>Community.</b>
		The Paulinian is a responsible family member and citizen, concerned with building communities, promotion of people, justice, and peace, and the protection of the environment.</h4>

		<h4><b>Charism.</b>The Paulinian develops his/her gifts/talents to be put in the service of the community, he/she strives to grow and improve daily, always seeking the better and finer things, and the final good.</h4>

		<h4><b>Charity.</b>
		Urged on by the love of Christ, the Paulinian is warm, hospitable, and "all to all", especially to the underprivileged.</h4>

		<p>Thus, Paulinian Education is committed to the formation of self-directed Catholic Filipino men and women who find fulfillment in intelligent fellowship and responsible leadership in meeting their responsibilities to God, country, and fellowmen.</p>
		</div>
	</div>

			<div class="container" style="margin-top: 10px">
				<div class="footer">
			 	 	<div class="jumbotron">
			 	 	<p style="text-align: center;margin-top: -40px"> Accreditations </p>
			 	 	<div class="row">
			 		 	<div class="col-sm-2" style="margin-right: 35px"><img src="image/iso.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
			    			<h4 style="text-align: center">ISO</h4>
			    		</div>

			    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/iao.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
			    			<h4 style="text-align: center">IAO</h4>
			    		</div>

			    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/aasbi.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
			    			<h4 style="text-align: center">AASBI</h4>
			    		</div>

			    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/paascu.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
			    			<h4 style="text-align: center">PAASCU</h4>
			    		</div>

			    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/pacucoa.png" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
			    			<h4 style="text-align: center">PACUCOA</h4>
			    		</div>
			  		</div>	
			</div>


</body>
</html>